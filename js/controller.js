// window.onscroll = function(e) {
//     // print "false" if direction is down and "true" if up

//     elemArr = ["#home", "#einfuehrung", "#machine", "#useCases", "#etl", "#machineLearning", "#results"]

//     if (this.oldScroll > this.scrollY) {

//         for (let i = 0; i < elemArr.length; i++) {
//             var top_of_element = $(elemArr[i]).offset().top;
//             var bottom_of_element = $(elemArr[i]).offset().top + $(elemArr[i]).outerHeight();
//             var bottom_of_screen = $(window).scrollTop() + window.innerHeight;
//             var top_of_screen = $(window).scrollTop();

//             //if ((bottom_of_screen > top_of_element) && (top_of_screen < bottom_of_element)) {
//             if (top_of_screen - top_of_element < 10) {
//                 console.log(elemArr[i] + ": " + elemArr[i - 1] + " befindet sich darüber")

//                 //Koordinaten des Elements darunter
//                 // var top_of_element_bottom = $(elemArr[i - 1]).offset().top;
//                 // var bottom_of_element_bottom = $(elemArr[i - 1]).offset().top + $(elemArr[i]).outerHeight();

//                 // if ((bottom_of_screen > top_of_element_bottom) && (top_of_screen < bottom_of_element_bottom)) {
//                 //     $([document.documentElement, document.body]).stop(true, false).animate({
//                 //         scrollTop: $(elemArr[i - 1]).offset().top
//                 //     }, 50);
//                 // }

//             }
//         }

//     } else {

//         for (let i = 0; i < elemArr.length; i++) {
//             var top_of_element = $(elemArr[i]).offset().top;
//             var bottom_of_element = $(elemArr[i]).offset().top + $(elemArr[i]).outerHeight();
//             var bottom_of_screen = $(window).scrollTop() + window.innerHeight;
//             var top_of_screen = $(window).scrollTop();

//             if ((bottom_of_screen > top_of_element) && (top_of_screen < bottom_of_element)) {
//                 console.log(elemArr[i] + ": " + elemArr[i + 1] + " befindet sich darunter")

//                 //Koordinaten des Elements darunter
//                 var top_of_element_bottom = $(elemArr[i + 1]).offset().top;
//                 var bottom_of_element_bottom = $(elemArr[i + 1]).offset().top + $(elemArr[i]).outerHeight();

//                 if ((bottom_of_screen > top_of_element_bottom) && (top_of_screen < bottom_of_element_bottom)) {
//                     $([document.documentElement, document.body]).stop(true, false).animate({
//                         scrollTop: $(elemArr[i + 1]).offset().top
//                     }, 50);
//                 }

//             }
//         }
//     }

//     this.oldScroll = this.scrollY;
// }

$(document).ready(function() {
    // Add smooth scrolling to all links
    $("a").on('click', function(event) {

        // Make sure this.hash has a value before overriding default behavior
        if (this.hash !== "") {
            // Prevent default anchor click behavior
            event.preventDefault();

            // Store hash
            var hash = this.hash;

            // Using jQuery's animate() method to add smooth page scroll
            // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 800, function() {

                // Add hash (#) to URL when done scrolling (default click behavior)
                window.location.hash = hash;
            });
        } // End if
    });
});